'use strict';

angular.module('myApp.controllers.AppController', ['ngResource'])
    .controller('AppController', function ($scope, AppPropertiesService, ConstantsService) {
		init();
		function init() {
			AppPropertiesService.get(function(data){
				$scope.props = {
						version: data.version,
						appName: data.appName
				};
			});
			$scope.constants = ConstantsService.get();
		};
    });