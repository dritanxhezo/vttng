'use strict';

angular.module('myApp.controllers.TitleController', ['ngResource', 'ui.router'])
    .controller('TitleController', function ($scope, $stateParams) {
		init();
		function init() {
//			$scope.collVin = $stateParams.vin;
		};
		$scope.openTitleIssueDateCalendar = function(e) {
			e.preventDefault();
			e.stopPropagation();
			$scope.titleIssueDateCalendarOpen = true;
		};
		$scope.openTitleReceiveDateCalendar = function(e) {
			e.preventDefault();
			e.stopPropagation();
			$scope.titleReceiveDateCalendarOpen = true;
		};		
	
//	    $scope.$on("loanLoadedEvent", function(e){
//			var selColl = _.find($scope.loan.loanCollaterals, function(coll) {return coll.vin==$scope.collVin;});
//			$scope.selectedTitle = selColl.loanTitles[0].title;
//	    });
//		$scope.$watch('reportsTree.currentNode.id', function(newVal, oldVal){
//			
//		});
		$scope.$on("loanLoadedEvent", updatePage);
		$scope.$on("$stateChangeSuccess", updatePage);
		function updatePage() {
			$scope.collVin = $stateParams.vin;
			if (angular.isUndefined($scope.loan) || $scope.loan == null) return;
			var selColl = _.find($scope.loan.loanCollaterals, function(coll) {return coll.vin==$scope.collVin;});
			$scope.selectedTitleDoc = selColl.loanTitles[0];
	    }		
    });