'use strict';

angular.module('myApp.controllers.MainController', [])
	.factory('httpInterceptor', function httpInterceptor ($q, $window, $location) {
	  return function (promise) {
	      var success = function (response) {
	          return response;
	      };
	
	      var error = function (response) {
	          if (response.status === 401) {
	              $location.url('/login');
	          }	
	          return $q.reject(response);
	      };	
	      return promise.then(success, error);
	  };
	})
	.controller('MainController', function ($scope, $location, UserService, UserInfoService) {  //, AppPropertiesService
		init();
		function init() {
			UserService.get(function(data){
				$scope.user = data;
//				angular.forEach($scope.user.assignedOrgs, function(org) {
//					if(org.isDefault==true){
//						$scope.selectedOrg = org;
//					}
//				});
			});
			
			UserInfoService.organizations(function(data){
				$scope.orgs = data;
				$scope.selectedOrg1 = null; // select the first item in the array by default
//				if (Array.isArray($scope.orgs) && $scope.orgs.length>0) {
//					$scope.selectedOrg1 = orgs[0];
//				}
				angular.forEach($scope.orgs, function(org) {
					if(org.default==true){
						$scope.selectedOrg1 = org;
					}
				});				
			});

			UserInfoService.recentdocuments(function(data){
				$scope.recentdocuments = data;
			});
			
			$scope.showSupport = false;
			$scope.supportTitle = 'Submit a support ticket';
			$scope.showNotifications = false;
			$scope.notificationTitle = 'All Notifications';
			$scope.showUserSettings = false;
			//$scope.userSettingsTitle = 'User Settings: ' + $scope.user.details.name;			
		};
		$scope.isCurrentView = function(path){
			return ($location.path().substr(0,path.length) == path);
		};
		$scope.getAllNotiCounts = function(list) {
			var total = 0;
			angular.forEach(list, function(item) {
				total += item.count;
			});
			return total;
		};
		$scope.showNotificationType = function(notiType) {
			$scope.notificationTitle = notiType + ' Notifications';
			console.log($scope.notificationTitle);
			$scope.showNotifications = true;
		};
//		$scope.selectOrg = function(org) {
//			if ((!org.disabled) && ($scope.selectedOrg.orgId != org.orgId)) {
//				$scope.selectedOrg = org;
//			}
//			changeOrg();
//		};
		$scope.selectOrg1 = function(org) {
			if ((!org.disabled) && ($scope.selectedOrg1.orgId != org.orgId)) {
				$scope.selectedOrg1 = org;
			}
			changeOrg();
		};
		
		function changeOrg() {
			console.log($scope.selectedOrg1);
		};
		$scope.selectDoc = function() {
			console.log($scope.recentDoc);
			$scope.recentDoc = '';
		};
		$scope.searchBy = function(searchByValue) {
			console.log($scope.searchFor + ', ' + searchByValue);
		};
	    
	    $scope.showSupportModal = function() {
	        $scope.showSupport = true;
	    };
	    $scope.submitSupport = function() {
	    	console.log('submitting a support ticket');
	        $scope.showSupport = false;
	    };
	    $scope.showUserSettingsModal = function() {
	    	$scope.userSettingsTitle = 'User Settings: ' + $scope.user.details.name;
	        $scope.showUserSettings = true;
	    };
	    $scope.submitUserSettings = function() {
	    	console.log('submitting user settings chages');
	        $scope.showUserSettings = false;
	    };
	});