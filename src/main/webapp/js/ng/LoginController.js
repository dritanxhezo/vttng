'use strict';

angular.module('myApp.controllers.LoginController', ['ngResource', 'ui.router', 'ngCapsLock'])
    .controller('LoginController', function ($scope, $state, LoginSvc) {
		init();
		function init() {
			$scope.wordPattern = /^\w+$/;												// single word
			$scope.passPattern = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])\w{6,}$/;   			// at least one number, one lowercase and one uppercase letter; at least six characters that are letters, numbers or the underscore
			$scope.pinPattern = /^(?=.*\d).{4,4}$/; 									// 4 digits
			$scope.emailPattern = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;			// email
		};
		$scope.login = function(){
			console.log('logging in:' + $scope.userId + ':' + $scope.password + ":" + $scope.pin);
 			//var credentials = {userId: $scope.userId, password: $scope.password, pin: $scope.pin};			
// 			var success = function (data, headers) {
// 				var token = data.token;
// 				console.log(token);
// 				console.log(headers);
// 				//api.init(token);
// 				//$cookieStore.put('token', token);
// 				$state.go('auth.home');
// 			};
// 			var error = function () {
// 				// TODO: apply user notification here..
// 			};
// 			LoginSvc.login(credentials, success, error);		
			
			LoginSvc.login({userId: $scope.userId, password: $scope.password, pin: $scope.pin}, 
				function(data, status, headers, config) {
					console.log('success');
					$state.go('auth.home');
				}, function() {
					console.log('failed');
				}
			);			

//			LoginSvc.login({userId: $scope.userId, password: $scope.password, pin: $scope.pin}).
//				$promise.then(function(result) {
//					console.log('suceeded' + result);
//					$state.go('auth.home');
//				}, function() {
//					console.log('failed');
//				});

			
//			LoginSvc.login({userId: $scope.userId, password: $scope.password, pin: $scope.pin}).
//				success(function(data, status, headers, config) {
//					// this callback will be called asynchronously
//					// when the response is available
//					console.log('success');
//					$state.go('auth.home');
//				}).
//				error(function(data, status, headers, config) {
//					// called asynchronously if an error occurs
//					// or server returns response with an error status.
//					console.log('error');
//				});
				
//				function(authToken) {
//					$scope.authToken = authToken;
//					$state.go('auth.home');
//				}
//				);
		};
		$scope.recoverId = function(){
			console.log('recovering id: ' + $scope.email);
			$state.go('nonauth.login');
		};		
		$scope.recoverPass = function(){
			console.log('recovering pass: ' + $scope.userId + ":" + $scope.email);
			$state.go('nonauth.login');
		};
    });