'use strict';

angular.module('myApp.services', ['ngResource'])
	//.value('version', '0.1')
	//.value('appName', 'VINtek TIME')
	.factory('LoginSvc', function($resource) {
		return $resource('service/login', {}, {
			login:  { method: 'POST', url: 'service/login/login'},
			logout: { method: 'POST', url: 'service/login/logout'},
			forgotuserid: { method: 'POST', url: 'service/login/forgotuserid'}, 
			forgotpass: { method: 'POST', url: 'service/login/forgotpass'},
			resetpass: { method: 'POST', url: 'service/login/resetpass'},
	    });
	})
	.factory('UserService', function($resource) {
		return $resource('data/userInfo.json');
	})
	.factory('UserInfoService', function($resource) {
		return $resource('service/userinfo', {}, {
			announcements: { method: 'GET', url: 'service/userinfo/announcements', params: {id: '@id'}, isArray: true},
			organizations: { method: 'GET', url: 'service/userinfo/organizations', isArray: true},
			recentdocuments: { method: 'GET', url: 'service/userinfo/recentdocuments', isArray: true}
		});
	})
	.factory('AppPropertiesService', function($resource) {
		return $resource('data/appProperties.json');
	})
//	.factory('ReportsService', function($resource) {
//		return $resource('data/reports.json');
//	});	
	.factory('ReportSvc', function($resource) {
		return $resource('service/report/:id', {}, {
			list:  { method: 'GET', isArray: true },
			create: { method: 'POST'},
	        getTemplate: { method: 'GET', isArray: true },
	        runReport: { method: 'POST', url: 'service/report/runReport'}, 
	        update: { method: 'PUT', params: {id: '@id'}},
	        remove: { method: 'DELETE', params: {id: '@id'}}
	    });
	})
	.factory('LoanService', function($resource) {
		return $resource('service/loan/:id', {}, {
			loadLoan: { method: 'GET', params: {id: '@id'}, isArray: false},
			create: { method: 'POST'},
			update: { method: 'PUT', params: {id: '@id'}},
	        remove: { method: 'DELETE', params: {id: '@id'}}
		});
	})
	.factory('ConstantsService', function($resource) {
		return $resource('data/constants.json');
	})	
	.factory('FilterOprSvc', function($resource) {
		return $resource('data/filterOperators.json');
	})
	.factory('UserRolesSvc', function($resource) {
		return $resource('data/userRoles.json');
	})
	;