'use strict';

angular.module('myApp.controllers.LoanController', ['ngResource', 'ui.router'])
    .controller('LoanController', function ($scope) { //$state, $stateParams, LoanService
		init();
		function init() {
//			$scope.accountId = $stateParams.accountId;
//			LoanService.loadLoan({id: $scope.accountId}, function(loan){
//				$scope.loan = loan;
//				$scope.selectedCollateralIndex = 0;
//				$scope.selectedCollateral = $scope.loan.loanCollaterals[0];
//            });
			$scope.currencyPattern = /^(?=.*\d).{1,2}$/;
		};
		
		$scope.openLoanStartDateCalendar = function(e) {
			e.preventDefault();
			e.stopPropagation();
			$scope.loanStartCalendarOpen = true;
		};
		$scope.openLoanEndDateCalendar = function(e) {
			e.preventDefault();
			e.stopPropagation();
			$scope.loanEndCalendarOpen = true;
		};
		$scope.openCollStartDateCalendar = function(e) {
			e.preventDefault();
			e.stopPropagation();
			$scope.collStartCalendarOpen = true;
		};
		
		$scope.selectPhoneType = function(phoneType) {
			$scope.loan.ownerPhoneType = phoneType.id;
		}
		$scope.selectPhone2Type = function(phoneType) {
			$scope.loan.ownerPhone2Type = phoneType.id;
		}
		
	    $scope.$on("loanLoadedEvent", function(e){
	    	$scope.selectCollateral(0);
	    });
	    
		$scope.selectCollateral = function(index) {
			$scope.selectedCollateralIndex = index;
			$scope.selectedCollateral = $scope.loan.loanCollaterals[index];
		}
		
		$scope.deleteItem = function(index) {
			if (confirm('Are you sure you want to delete this collateral?')) {
				$scope.loan.loanCollaterals.splice(index,1);
				//if ($scope.loan.loanCollaterals.length=0){
				//	$scope.selectedCollateral = createLoanCollateral();
				//} else 
				if (index==$scope.selectedCollateralIndex) {
					$scope.selectedCollateralIndex = 0;
				}
			}
		}
		function createLoanCollateral(){
			return null;
		}
    });