'use strict';

angular.module('myApp.controllers.AccountController', ['ngResource'])
    .controller('AccountController', function ($scope, $stateParams, LoanService) {
		init();
		function init() {
			$scope.accountId = $stateParams.accountId;
			LoanService.loadLoan({id: $scope.accountId}, function(loan){
				$scope.loan = loan;
				$scope.$broadcast("loanLoadedEvent");
//				$scope.selectedCollateralIndex = 0;
//				$scope.selectedCollateral = $scope.loan.loanCollaterals[0];
            });			
		};
    });