'use strict';

angular.module('myApp', [
  'ui.bootstrap', 'ui.bootstrap.datepicker','isoDatePicker',
  'blockUI',
  'ui.router',
  'myApp.filters',
  'myApp.services',
  'myApp.controllers.AppController',
  'myApp.controllers.LoginController',
  'myApp.controllers.MainController',
  'myApp.controllers.HomeController',
  'myApp.controllers.FindLoanController',
  'myApp.controllers.FindTitleController',
  'myApp.controllers.ReportsController',
  'myApp.controllers.AccountController',
  'myApp.controllers.LoanController',
  'myApp.controllers.TitleController',
  'myApp.controllers.WorkflowController',
  'myApp.controllers.AttachmentsController',
  'myApp.controllers.UsersController',
  'myApp.controllers.ImportController',
  'angularTreeview',
  'ngPanelSlider',
  'ngModal',
  'ngScrollSpy',
  'wu.masonry',
  'ngSanitize'
])
.config(function($stateProvider, $urlRouterProvider, $httpProvider) {
	  //$httpProvider.responseInterceptors.push('httpInterceptor');
	  $urlRouterProvider.otherwise("/login");
	  $stateProvider
	  	.state('nonauth', {url: "/login", abstract: true, templateUrl: "partials/login.html"})
	  	.state('nonauth.login', {url: "", templateUrl: "partials/login.login.html"})
	  	.state('nonauth.recoverId', {url: "/recoverId", templateUrl: "partials/login.recoverId.html"})
	  	.state('nonauth.recoverPass', {url: "/recoverPass", templateUrl: "partials/login.recoverPass.html"})
	  	.state('auth', {url: "/app", templateUrl: "partials/main.html"})
	    .state('auth.home', {url: "/home", templateUrl: "partials/home.html"})
	    .state('auth.new_loan', {url: "/new_loan", templateUrl: "partials/loan.html"})
		.state('auth.new_title', {url: "/new_title", templateUrl: "partials/title.html"})
		.state('auth.find_loan', {url: "/find_loan", templateUrl: "partials/find_loan.html"})
		.state('auth.find_title', {url: "/find_title", templateUrl: "partials/find_title.html"})
		.state('auth.reports', {url: "/reports", templateUrl: "partials/reports.html"})
		.state('auth.users', {url: "/users", templateUrl: "partials/users.html"})
		.state('auth.import', {url: "/import", templateUrl: "partials/import.html"})
		.state('auth.help', {url: "/help/:topicId", templateUrl: "partials/help.html"})
		.state('auth.account', {url: "/account/:accountId", templateUrl: "partials/account.html"})		
		.state('auth.account.loan', {url: "/loan", templateUrl: "partials/loan.html", 
		    onEnter: ['$stateParams', '$state', function ($stateParams, $state) {
		        //alert('account_id is just fine: ' + $stateParams.accountId);
		      }]		
		})
		.state('auth.account.title', {url: "/title/:vin", templateUrl: "partials/title.html"})
		.state('auth.account.workflow', {url: "/workflow", templateUrl: "partials/account.workflow.html"})
		.state('auth.account.lieninquiry', {url: "/lieninquiry", templateUrl: "partials/account.lieninquiry.html"})
		.state('auth.account.attachments', {url: "/attachments", templateUrl: "partials/account.attachments.html"})
		.state('auth.account.documents', {url: "/documents", templateUrl: "partials/account.documents.html"});
});
