'use strict';

angular.module('myApp.controllers.UsersController', ['ngResource', 'ui.router'])
    .controller('UsersController', function ($scope, $state, UserRolesSvc) {
		init();
		function init() {
//			$scope.userRoles = UserRolesSvc.get();
			
     	   $scope.roles = [
				           {id:1,name:'Client Administrator'},
				           {id:2,name:'Client\'s User Administrator'}, 
				           {id:3,name:'System Administrator'},
				           {id:4,name:'Configuration Administrator'},
				           {id:5,name:'Title Viewer',selected:true},
				           {id:6,name:'Internal Titling Agent'},
				           {id:7,name:'Client Titling Agent'},
				           {id:8,name:'Accountant'},
				           {id:9,name:'Accounting Manager'},
				           {id:10,name:'Data Uploader'},
				           {id:11,name:'Security Auditor'},
				           {id:12,name:'Workflow Only Editor',selected:true}
           ];
     	   
//			$scope.reports = ReportSvc.list(); //ReportsSvc.get();
//			$scope.filterOperators = FilterOprSvc.get();
//
//            $scope.gap = 5;
//            $scope.itemsPerPage = 5;
//            $scope.pagedItems = [];
//            $scope.currentPage = 0;
//			$scope.currTemplate = {fields: null,orgs: null,filters: [],newFilter: null,editingFilter: -1};
		};
    });