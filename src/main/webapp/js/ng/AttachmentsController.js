'use strict';

angular.module('myApp.controllers.AttachmentsController', ['ngResource'])
    .controller('AttachmentsController', function ($scope) {
		init();
		function init() {
			$scope.attachments = [
			                      {"titleid":"512002887630", "type":"sif", "thumbSrc":"data/images/512002887630-thumb.jpg", "src":"data/images/512002887630.jpg", "attachedOn":"1 jan"},
			                      {"titleid":"512002934671", "type":"title", "thumbSrc":"data/images/512002934671-thumb.jpg", "src":"data/images/512002934671.jpg", "attachedOn":"1 jan"},
			                      {"titleid":"512002887028", "type":"title", "thumbSrc":"data/images/512002887028-thumb.jpg", "src":"data/images/512002887028.jpg", "attachedOn":"1 jan"},
			                      {"titleid":"512010217531", "type":"title", "thumbSrc":"data/images/512010217531-thumb.jpg", "src":"data/images/512010217531.jpg", "attachedOn":"1 jan"},
			                      {"titleid":"514005917836", "type":"title", "thumbSrc":"data/images/514005917836-thumb.jpg", "src":"data/images/514005917836.jpg", "attachedOn":"1 jan"}
			                      ];
	        $scope.selectedAttachment = null;
	        $scope.showAttachmentModal = false;
	        $scope.attachmentModalTitle = '';
	        $scope.newAtachment = {};
	        
            $scope.addAttachment = function () {
            	console.log("adding another attachment");
                //$scope.attachments.push($scope.newAtachment);
                //$scope.newAtachment = null;
            };
            $scope.removeAttachment = function(index) {
            	$scope.attachments.splice(index,1);
            };            
            $scope.viewAttachment = function(index) {
            	$scope.selectedAttachment = $scope.attachments[index];
            	$scope.attachmentModalTitle = 'Title:' + $scope.selectedAttachment.titleid + ' | Type: ' + $scope.selectedAttachment.type + ' | Attached On: ' + $scope.selectedAttachment.attachedOn;
            	$scope.showAttachmentModal = true;
            };
            
		};
    });