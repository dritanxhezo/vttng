var stringToTimestamp = angular.module('isoDatePicker', [])
.directive('datepickerPopup', function () {
	  function link(scope, element, attrs, ngModel) {
	    // View -> Model
	    ngModel.$parsers.push(function (value) {
	    	var date = new Date(value);
	    	return date.toISOString();
	    });
	  }

	  return {
	    restrict: 'A',
	    require: 'ngModel',
	    link: link
	  };
	});