/* Panel Slider - Slides a collection of panels
 * Author: Dritan Xhezo
 */
(function ($) {
	var scrollStrip;
	var stripWidth;
	var scrollLeft;
	var scrollRight;
	var scrollStep;
	
    $.fn.panelSlider = function(options) {
		var settings = $.extend({
			itemClass: 'item',
			selectable: true,
			selectableClass: 'selected'
		}, options);
        var container = this;
        var item = container.find('.' + settings.itemClass);
        //container.css('height', item.css('height') + container.css('padding-top') + container.css('padding-bottom') + container.css('border-top-width') + container.css('border-bottom-width'));
        //container.css('height', item.outerHeight(true) + parseInt(container.css('padding-top')) + parseInt(container.css('border-top-width')));
        //container.innerHeight(item.outerHeight(true) + parseInt(container.css('padding-top')) + parseInt(container.css('padding-bottom')) + parseInt(container.css('border-top-width')) + parseInt(container.css('border-bottom-width')));
        
        //container.css('height', item.outerHeight(true));
		scrollStrip = container.find('.scrollstrip');
		scrollLeft = this.find('.scroll-left');
		scrollRight = this.find('.scroll-right');
		
		//stripWidth = scrollStrip.children().length * parseInt(scrollStrip.children().first().outerWidth(true));
		stripWidth = item.length * parseInt(item.first().outerWidth(true));
		scrollStrip.css('width', stripWidth);
		
		container.find('.scrollclip').css('clip', 'rect(0px,'+ container.css('width') + ',' + container.css('height') + ',0px)');
		scrollStep = parseInt(container.innerWidth());
		
		//enableScrolling();
		scrollLeft.click(function(){psScroll(container,'left');});
		scrollRight.click(function(){psScroll(container,'right');});
		
		if (settings.selectable) {
//			scrollStrip.children().click(function(){
//				scrollStrip.children('.'+settings.selectableClass).removeClass(settings.selectableClass);
//				$(this).addClass(settings.selectableClass);
//			});
			item.click(function(){
			//scrollStrip.find('.item').click(function(){
				//scrollStrip.find('.'+settings.selectableClass).removeClass(settings.selectableClass);
				$(this).siblings('.'+settings.selectableClass).removeClass(settings.selectableClass);
				$(this).addClass(settings.selectableClass);
			});
			
		};
		
		//use underscore's debounce function to wait for 500 millis until triggering the function.
		$(window).resize(_.debounce(function() {
			scrollStep = parseInt(container.innerWidth());
			container.find('.scrollclip').css('clip', 'rect(0px,'+ container.css('width') + ',' + container.css('height') + ',0px)');
		}, 300));
        return this;
    };
 
 	var psScroll = function(container, direction){
		var moveDirection = (direction=='left')?1:-1;
		var curPos = parseInt(scrollStrip.css('left'));
		var delta = moveDirection*scrollStep;
		var newPos = curPos + scrollStep*moveDirection;
	
		if (!((newPos>0) || (newPos<stripWidth*-1))){
			scrollStrip.animate({left: '+='+delta}, 500);
		}
		//enableScrolling();
		scrollLeft.toggleClass('disabled', (newPos>=0));
		scrollRight.toggleClass('disabled', (newPos<=(stripWidth*-1 + scrollStep)));
	};
	
	var enableScrolling = function() {
		var curPos = parseInt(scrollStrip.css('left'));
		var newPosLeft = curPos + scrollStep;
		var newPosRight = curPos - scrollStep;
		scrollLeft.toggleClass('disabled', (curPos>=0));
		scrollRight.toggleClass('disabled', (curPos<=(stripWidth*-1 + scrollStep)));
	}
}( jQuery ));