angular.module('ngPanelSlider', [])
	.controller('Controller', ['$scope', function($scope) {
	  $scope.collateral = {
		vin: '1FALP62W4WH128703',
		vehYear: '2004',
		vehMake: 'Mercedes',
		vehModel: 'Babylon',
		titleType: 'paper',
		titleState: 'PA',
		titleNumber: 'T100202A',
		lienStatus: 'perfected',
		collateralType: 'auto',
		titleArchived: 'archived'
	  };
	}])
	.directive('panelSlider', function() {
		return {
			restrict: 'AE',
			scope: {
				items: '='
			},
			template:   '<div class="panelSliderContainer" id="container_auto">' +
							'<div class="scrollclip">' +
								'<div class="scrollstrip">' +
								
									'<div class="item">' +
										'<span class="item-title">{{items.vin}}</span>' +
										'<div class="item-detail"><span>{{items.vehYear}} {{items.vehMake}} {{items.vehModel}}</span></div>' +
										'<div class="item-detail"><span class="title-{{items.titleType}}">   {{items.titleState}} {{items.titleNumber}}</span></div>' +
										'<div class="item-status">' +
											'<div class="item-status-elm"><span class="lien-status {{items.lienStatus}}"></span></div>' +
											'<div class="item-status-elm"><span class="veh-type {{items.collateralType}}"></span></div>' +
											'<div class="item-status-elm"><span class="title-{{items.titleArchived}}"></span></div>' +
										'</div>' +
										'<div class="item-close"></div>' +
									'</div>' +
									
								'</div>' +
							'</div>' +
							'<div class="div_scrollarrow scroll-left disabled"><img src="images/blank.png"/></div>' +
							'<div class="div_scrollarrow scroll-right"><img src="images/blank.png"/></div>' +
						'</div>'
	  };
	});
	
angular.module('myApp', ['ngPanelSlider']);