angular.module('ngPanelSlider', [])
	.directive('panelSlider', function() {
		return {
			template:   '<div class="panelSliderContainer">' +
							'<div class="scrollclip">' +
								'<div class="scrollstrip">' +
								   '<ng-transclude></ng-transclude>' +
								'</div>' +
							'</div>' +
							'<div class="div_scrollarrow scroll-left disabled"><img src="images/blank.png"/></div>' +
							'<div class="div_scrollarrow scroll-right"><img src="images/blank.png"/></div>' +
						'</div>',			
			restrict: 'AE',
			scope: {
				items: '='
			},
			transclude: true,
			replace:true,
			link: function (scope, element, attributes) {
				element.panelSlider();
		}
	  };
	});