angular.module('ngPanelSlider', [])
	.controller('Controller', ['$scope', function($scope) {
	  $scope.collateral = {items:[
		    {vin: '1FALP62W4WH128703',
			year: '2004',
			make: 'Mercedes',
			model: 'Babylon',
			titleType: 'paper',
			titleState: 'PA',
			titleNumber: 'T100202A',
			lienStatus: 'perfected',
			collateralType: 'auto',
			archive: false},
			
		    {vin: '1FALP62W4WH128703',
			year: '2005',
			make: 'Picka',
			model: 'Bythka',
			titleType: 'paper',
			titleState: 'PA',
			titleNumber: 'T100202A',
			lienStatus: 'perfected',
			collateralType: 'auto',
			archive: true}
	    ]};
	}])
	.directive('panelSlider', function() {
		return {
			template:   '<div class="panelSliderContainer">' +
							'<div class="scrollclip">' +
								'<div class="scrollstrip">' +
								   '<ng-transclude></ng-transclude>' +
								'</div>' +
							'</div>' +
							'<div class="div_scrollarrow scroll-left disabled"><img src="images/blank.png"/></div>' +
							'<div class="div_scrollarrow scroll-right"><img src="images/blank.png"/></div>' +
						'</div>',			
			restrict: 'AE',
			scope: {
				items: '='
			},
			transclude: true,
			replace:true,
			link: function (scope, element, attrs) {
				element.panelSlider();
			}
	  };
	});
	
angular.module('myApp', ['ngPanelSlider']);