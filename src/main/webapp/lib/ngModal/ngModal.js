var mymodal = angular.module('ngModal', [])
  .directive('modal', function () {
    return {
      template: '<div class="modal fade in" role="dialog">' + 
          '<div class="modal-dialog">' +
            '<div class="modal-content">' + 
              '<div class="modal-header">' + 
                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true" ng-hide="closable">&times;</button>' + 
                '<h4 class="modal-title" ng-show="title && title.length">{{ title }}</h4>' + 
              '</div>' + 
              '<div class="modal-body" ng-transclude></div>' + 
            '</div>' + 
          '</div>' + 
        '</div>',
      restrict: 'E',
      transclude: true,
      replace:true,
      scope: true,
      link: function postLink(scope, element, attrs) {
        scope.title = scope.$parent[attrs.title];
        scope.closable = (attrs.closable=="false");

        scope.$watch(attrs.visible, function(value){
          if(value == true)
            $(element).modal('show');
          else
            $(element).modal('hide');
        });
        
        scope.$watch(attrs.title, function(value){
        	scope.title = scope.$parent[attrs.title];
        });

        $(element).on('shown.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = true;
          });
        });

        $(element).on('hidden.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = false;
          });
        });
      }
    };
  });