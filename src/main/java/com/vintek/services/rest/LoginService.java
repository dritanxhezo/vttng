package com.vintek.services.rest;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.servlet.SessionCookieConfig;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.vintek.database.model.user.User;
import com.vintek.services.controller.LoginController;
import com.vintek.services.model.Login;
import com.vintek.services.rest.ServiceException;
import com.vintek.services.security.SecuredService;

@Path("/login")
@Stateless
//@SecuredService
public class LoginService {

    private static final String SESSION_USER = "user";
	//private static final String ACCESS_TOKEN = "X-Access-Token";
	private static final String ACCESS_TOKEN = "X-XSRF-TOKEN";
	
	private static final String XSRF_TOKEN_COOKIE = "XSRF-TOKEN";

	@Inject private Logger log;
	@Inject LoginController controller;
	
    @Context UriInfo uriInfo;
    @Context ServletContext servletContext;
    
	@POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)	
	@Path("/login")
	//public Response login(@Context HttpServletRequest request) throws ServiceException {
	public Response login(Login login, @Context HttpServletRequest request, @Context HttpServletResponse response) throws ServiceException {
		String token = controller.login(login, request);

		// set the csrf token
//		String contextPath = servletContext.getContextPath();
//        SessionCookieConfig sessionCookieConfig = servletContext.getSessionCookieConfig();
//        String path = sessionCookieConfig.getPath();
//        String domain = null; //sessionCookieConfig.getDomain();  // see: http://stackoverflow.com/questions/25578872/ie-chrome-not-storing-cookies-from-resteasy
//        int age = sessionCookieConfig.getMaxAge();
//        boolean secureCookie = sessionCookieConfig.isSecure();
//        
//        NewCookie cookie = new NewCookie(XSRF_TOKEN_COOKIE, token, contextPath, path, domain, age, secureCookie);
//        //cookies.add(cookie.toString() + ";HttpOnly"); //Hacky way to introduce HttpOnly
//        return Response.ok().cookie(cookie).build();
        
		//response.setHeader(ACCESS_TOKEN, token);
		//response.addCookie(new Cookie(XSRF_TOKEN_COOKIE, token));
		//return Response.status(Response.Status.OK).entity(token).build();
		return Response.ok().build();
		//return Response.status(Response.Status.OK).entity("Logged in as " + request.getParameter("userId")).build();
		//return Response.created(uriInfo.getAbsolutePathBuilder().path(request.getParameter("userId")).build()).entity("Logged in as " + request.getParameter("userId")).build();
		
	}
	
	@POST
	@Path("/whoami")
	public Response whoami(@Context HttpServletRequest request) throws ServiceException {
		String userId = "";
		if (request.getHeader(ACCESS_TOKEN).equals(request.getSession().getAttribute(ACCESS_TOKEN))) {
			User user = (User) request.getSession().getAttribute(SESSION_USER);
			userId = user.getUserID();
		}
		return Response.status(Response.Status.OK).entity("Logged in as " + userId).build();
	}	

	@POST
	@Path("/logout")
	public Response logout(@Context HttpServletRequest request) throws ServiceException {
		controller.logout(request);
		return Response.status(Response.Status.OK).entity("Logged out").build();
	}
	
	@POST
	@Path("/forgotpass")
	public Response forgotPassword(@Context HttpServletRequest request, @Context HttpServletResponse response) throws ServiceException {
		controller.forgotPassword(request);
		return Response.status(Response.Status.OK).entity("Forgot password for " + request.getParameter("userId") + " processed.").build();
	}

	@POST
	@Path("/resetpass")
	public Response resetPassword(@Context HttpServletRequest request, @Context HttpServletResponse response) throws ServiceException {
		controller.resetPassword(request);
		return Response.status(Response.Status.OK).entity("Reset password for " + request.getParameter("userId") + " processed.").build();
	}

	@POST
	@Path("/forgotuserid")
	public Response forgotUserId(@Context HttpServletRequest request, @Context HttpServletResponse response) throws ServiceException {
		controller.forgotUserId(request);
		return Response.status(Response.Status.OK).entity("Forgot userid for " + request.getParameter("email") + " processed.").build();
	}
}
