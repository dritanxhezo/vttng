package com.vintek.services.rest;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.vintek.database.model.loan.Loan;
import com.vintek.services.controller.LoanController;
import com.vintek.services.model.LoanCriteria;
import com.vintek.services.security.NeedsAccessToken;

@Path("/loan")
@NeedsAccessToken
@Stateless
public class LoanService {

    @Inject
    private Logger log;

    @Context
    UriInfo uriInfo;    
    
    @Inject
    private LoanController controller;

	@GET
	@Produces(APPLICATION_JSON)
//	@JsonView(Views.Public.class)
    public Response getLoans(LoanCriteria loanSearchCriteria) {
		final List<Loan> loans = controller.getLoans(loanSearchCriteria);
		return Response.ok(loans).build();
    }

	@GET
	@Path("/{id}")	
	@Produces(APPLICATION_JSON)
    public Response getLoanById(@PathParam("id") String id) throws ServiceException, NoResultException{   //:[0-9][0-9]*
		final Loan loan = controller.getLoan(id);
		return Response.ok(loan).build();
    }
	
	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	public Response createLoan(Loan loan) throws ConstraintViolationException, ValidationException, ServiceException {
		Loan loanCreated = controller.createLoan(loan);
		return Response.created(uriInfo.getAbsolutePathBuilder().path(loanCreated.getApplicationID()).build()).build();
    }
   
    @PUT
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    public Response updateLoan(Loan loan) throws ValidationException, ServiceException {
    	Loan updatedLoan = controller.update(loan);
		return Response.created(uriInfo.getAbsolutePathBuilder().path(updatedLoan.getApplicationID()).build()).build();
    }
	
    @DELETE
    @Path("/{id:[0-9][0-9]*}")
    public Response deleteLoan(@PathParam("id") int id) throws ServiceException {
    	controller.delete(id);
        return Response.noContent().build();
    }
}
