package com.vintek.services.rest;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.vintek.database.model.loan.Title;
import com.vintek.database.model.report.ReportField;
import com.vintek.services.controller.TitleController;
import com.vintek.services.controller.ReportController;
import com.vintek.services.model.TitleCriteria;
import com.vintek.services.model.ReportCriteria;
import com.vintek.services.model.ReportResult;
import com.vintek.services.model.ReportTree;
import com.vintek.services.security.NeedsAccessToken;

@Path("/title")
@NeedsAccessToken
@Stateless
public class TitleService {

    @Inject
    private Logger log;

    @Context
    UriInfo uriInfo;    
    
    @Inject
    private TitleController controller;

	@GET
	@Produces(APPLICATION_JSON)
    public Response getTitles(TitleCriteria titleSearchCriteria) {
		final List<Title> titles = controller.getTitles(titleSearchCriteria);
		return Response.ok(titles).build();
    }

	@GET
	@Path("/{id}")	
	@Produces(APPLICATION_JSON)
    public Response getTitleById(@PathParam("id") String id) {   //:[0-9][0-9]*
		final Title title = controller.getTitle(id);
		return Response.ok(title).build();
    }

//	@POST
//	@Path("/runReport")
//	@Consumes(APPLICATION_JSON)
//	@Produces(APPLICATION_JSON)
//	public Response runReport(ReportCriteria reportCriteria) throws ConstraintViolationException, ValidationException, ServiceException, NoResultException {
//		ReportResult reportResult = controller.runReport(reportCriteria);
//		return Response.ok(reportResult).build();
//    }
	
	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	public Response createTitle(Title title) throws ConstraintViolationException, ValidationException, ServiceException {
		Title titleCreated = controller.createTitle(title);
		return Response.created(uriInfo.getAbsolutePathBuilder().path(Integer.toString(titleCreated.getId())).build()).build();
    }
   
    @PUT
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    public Response updateTitle(Title title) throws ValidationException, ServiceException {
    	Title updatedTitle = controller.update(title);
		return Response.created(uriInfo.getAbsolutePathBuilder().path(Integer.toString(updatedTitle.getId())).build()).build();
    }
	
    @DELETE
    @Path("/{id:[0-9][0-9]*}")
    public Response deleteTitle(@PathParam("id") int id) throws ServiceException {
    	controller.delete(id);
        return Response.noContent().build();
    }
}
