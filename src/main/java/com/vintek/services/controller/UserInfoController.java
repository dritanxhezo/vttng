package com.vintek.services.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import com.vintek.database.model.user.Organization;
import com.vintek.database.model.user.User;
import com.vintek.database.model.user.UserOrganizationsView;
import com.vintek.database.model.user.UserRecentDocument;
import com.vintek.database.service.MojaveDB;
import com.vintek.services.model.UserAnnouncement;
import com.vintek.services.rest.ServiceException;


public class UserInfoController {

	private static final String SESSION_USER = "user";

	@Inject
	private Logger log;

	@Inject
	private MojaveDB mojaveDB;
	
	@Context
    HttpServletRequest request;
	
	public List<UserAnnouncement> getAnnouncements() throws ServiceException {
		List<UserAnnouncement> announcements = new ArrayList<UserAnnouncement>();
		//try {			
			UserAnnouncement announcement = new UserAnnouncement();
			announcement.setId("1");
			announcement.setMessage("message 1");
			announcement.setType("lien_inquiry");
			announcement.setRead(false);
			announcements.add(announcement);
		//} catch (Exception e) {			
			//throw new ServiceException(Response.Status.INTERNAL_SERVER_ERROR, "An Exception occurred while trying to get the announcements.");//, e);
			//throw new ServiceException("An Exception occurred while trying to get the announcements.");//, e);
			
		//}
		return announcements;
	}

	public UserAnnouncement getAnnouncement(int id) throws ServiceException {
		UserAnnouncement announcement = new UserAnnouncement();
		announcement.setId("1");
		announcement.setMessage("message 1");
		announcement.setType("lien_inquiry");
		announcement.setRead(false);
		return announcement;
	}

	public List<UserOrganizationsView> getAssignedOrganizations(HttpServletRequest request) {
		User loggedUser = (User) request.getSession().getAttribute(SESSION_USER);
		List<UserOrganizationsView> userOrgs = mojaveDB.findWithNamedQuery("Organizations.ByUserId", loggedUser.getUserID());
		return userOrgs;
	}

	public List<UserRecentDocument> getRecentDocuments(HttpServletRequest request) {
		User loggedUser = (User) request.getSession().getAttribute(SESSION_USER);
		List<UserRecentDocument> userRecentDocuments = mojaveDB.findWithNamedQuery("RecentDocuments.ByUserId", Integer.toString(loggedUser.getId()));
		return userRecentDocuments;
	}
}
