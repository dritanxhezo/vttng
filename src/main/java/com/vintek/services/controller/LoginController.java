package com.vintek.services.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;

import com.vintek.database.model.user.Privilege;
import com.vintek.database.model.user.User;
import com.vintek.database.model.user.UserPermission;
import com.vintek.database.service.MojaveDB;
import com.vintek.services.model.Login;
import com.vintek.services.rest.ServiceException;
import com.vintek.services.security.SecuredService;
import com.vintek.services.security.SecurityManager;
import com.vintek.services.security.SecurityToken;
import com.vintek.util.Encrypter;
import com.vintek.util.LogPerformance;

/**
 * @author dritan.xhezo
 */
//@RequestScoped
//@SecuredService 
@LogPerformance
public class LoginController {

	@Inject
	private Logger log;

	@Inject
	private MojaveDB mojaveDB;

	@Inject
	private SecurityManager securityManager;

	private static final String SESSION_USER = "user";
	//private static final String ACCESS_TOKEN = "X-Access-Token";
	private static final String ACCESS_TOKEN = "X-XSRF-TOKEN";

	public String login(Login login, HttpServletRequest request) throws ServiceException {
		String sid = "";
		validateLogin(login);

		Encrypter encrypter = new Encrypter();
		String encPassword = encrypter.encrypt(login.getPassword());
		User loggedUser = null;

		try {
			List<User> users = mojaveDB.findWithNamedQuery("User.ByUserId", login.getUserId());
			for (User user : users) {
				if (encPassword.equals(user.getPasswordEnc()) && login.getPin().equals(user.getPin()) && user.isEnabled()) {
					loggedUser = user;
				}
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, "Could not load the user from the database" + e.getMessage(), e);
			throw new ServiceException(Response.Status.INTERNAL_SERVER_ERROR, "An exception occurred while servicing this request");
		}

		if (loggedUser != null) {
			Map<String, List<Privilege>> privileges = createPrivilegesMap(loggedUser);
			loggedUser.setPrivilegesMap(privileges);
			loggedUser.setCurrentOrgId(loggedUser.getDefaultOrgID());
			
			SecurityToken secToken = new SecurityToken(loggedUser.getUserID(), loggedUser.getCurrentOrgId(), null);
			SecurityManager.placeSecurityToken(secToken);
			sid = secToken.getSid();
			request.getSession().setAttribute(SESSION_USER, loggedUser);
			request.getSession().setAttribute(ACCESS_TOKEN, sid);
		} else {
			throw new ServiceException(Response.Status.UNAUTHORIZED, "The credentials provided are not valid.");
		}
		return sid;
	}

	private Map<String, List<Privilege>> createPrivilegesMap(User loggedUser) {
		Map<String, List<Privilege>> privileges = new HashMap<String, List<Privilege>>();			
		for (UserPermission p: loggedUser.getUserPermissions()) {
			String orgId = Integer.toString(p.getOrganizationID());
			if (!privileges.containsKey(orgId)) {
				privileges.put(orgId, new ArrayList<Privilege>());
			}
			privileges.get(orgId).add(p.getPrivilege());
		}
		return privileges;
	}

	private void validateLogin(Login login) throws ServiceException {
		if (login.getUserId() == null  || login.getUserId().trim().isEmpty()
			|| login.getPassword() == null || login.getPassword().trim().isEmpty()
			|| login.getPin() == null || login.getPin().trim().isEmpty()) {
			throw new ServiceException(Response.Status.BAD_REQUEST, "Must provide non empty credentials.");
		}
	}

	public void logout(HttpServletRequest request) throws ServiceException {
		if (request.getSession() == null || request.getSession().getAttribute(SESSION_USER) == null) {
			throw new ServiceException(Response.Status.BAD_REQUEST, "Not logged in.");
		} else {
			request.getSession().invalidate();
			SecurityManager.clearSecurityToken();
		}
	}

	public void forgotPassword(HttpServletRequest request) throws ServiceException {
		// TODO send an email to reset the password
	}

	public void forgotUserId(HttpServletRequest request) throws ServiceException {
		// TODO send an email to remind of the user id
	}

	public void resetPassword(HttpServletRequest request) throws ServiceException {
		// TODO find the user / put the user in the forget password session key
		// / send back an id for the change password page ???
	}

}
