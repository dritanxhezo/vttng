package com.vintek.services.controller;

import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;

import com.vintek.database.model.loan.Loan;
import com.vintek.database.model.loan.Title;
import com.vintek.database.service.MojaveDB;
import com.vintek.services.model.TitleCriteria;
import com.vintek.util.LogPerformance;

@LogPerformance
public class TitleController {

	@Inject
	private Logger log;
	
	@Inject
	private MojaveDB mojaveDB;

	public List<Title> getTitles(TitleCriteria titleSearchCriteria) {
		return null;
	}

	public Title getTitle(String applicationId)  throws NonUniqueResultException, EntityNotFoundException, NoResultException {
		//Title title = (Title) mojaveDB.findWithNamedQuerySingleResult("Titles.ByTitleId", titleId);
		Title title = (Title) mojaveDB.findWithQuerySingleResult("SELECT t FROM Title as t join fetch t.loanTitles ltt join fetch ltt.loanTitles  WHERE l.applicationID=?1 ORDER BY l.id", applicationId);
		return title;
	}

	public Title createTitle(Title title) {
		// TODO Auto-generated method stub
		return null;
	}

	public Title update(Title title) {
		// TODO Auto-generated method stub
		return null;
	}

	public void delete(int id) {
		// TODO Auto-generated method stub
		
	}
}
