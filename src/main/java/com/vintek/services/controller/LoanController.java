package com.vintek.services.controller;

import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;

import com.vintek.database.model.loan.Loan;
import com.vintek.database.model.report.Report;
import com.vintek.database.service.MojaveDB;
import com.vintek.services.model.LoanCriteria;
import com.vintek.util.LogPerformance;

@LogPerformance
public class LoanController {

	@Inject
	private Logger log;
	
	@Inject
	private MojaveDB mojaveDB;

	public List<Loan> getLoans(LoanCriteria loanSearchCriteria) {
		return null;
	}

	public Loan getLoan(String applicationId)  throws NonUniqueResultException, EntityNotFoundException, NoResultException {
		//Loan loan = (Loan) mojaveDB.findWithNamedQuerySingleResult("Loans.ByApplicationId1", applicationId);
		Loan loan = (Loan) mojaveDB.findWithQuerySingleResult("SELECT l FROM Loan as l join fetch l.loanCollaterals c join fetch c.loanTitles WHERE l.applicationID=?1 ORDER BY l.id", applicationId);
		return loan;
	}

	public Loan createLoan(Loan loan) {
		// TODO Auto-generated method stub
		return null;
	}

	public Loan update(Loan loan) {
		// TODO Auto-generated method stub
		return null;
	}

	public void delete(int id) {
		// TODO Auto-generated method stub
		
	}
}
