package com.vintek.services.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletRequest;

import com.vintek.database.model.report.Report;
import com.vintek.database.model.report.ReportField;
import com.vintek.database.model.user.User;
import com.vintek.database.service.MojaveDB;
import com.vintek.services.model.ReportCriteria;
import com.vintek.services.model.ReportResult;
import com.vintek.services.model.ReportTree;
import com.vintek.util.LogPerformance;

/**
 * @author dritan.xhezo
 */
@LogPerformance
public class ReportController {
	@Inject
	private Logger log;
	
	@Inject
	private MojaveDB mojaveDB;	

	private static final String SESSION_USER = "user";
	
    // Report Type Constants
    public final static byte REPORT_LOAN = 0;
    public final static byte REPORT_TITLE = 1;
    public final static byte REPORT_TRANSACTION = 2;
    public final static byte REPORT_SEARCH_LOAN = 3;
    public final static byte REPORT_SEARCH_TITLE = 4;
    public final static byte REPORT_ADMIN = 5;
    public final static byte REPORT_ORG_ADMIN = 6;
    public final static byte REPORT_LIEN_INQUIRY = 7;
   
    // Archive Search Constants
    public final static byte ALL_ACCOUNTS = 0;
    public final static byte ACTIVE_ACCOUNTS = 1;
    public final static byte ARCHIVE_ACCOUNTS = 2;
    
	public final static byte TYPE_STRING = 0;
	public final static byte TYPE_NUMBER = 1;
	public final static byte TYPE_CURRENY = 2;
	public final static byte TYPE_DATE = 3;
	public final static byte TYPE_DATETIME = 4;
	public final static byte TYPE_BOOLEAN = 5;
	public final static byte TYPE_DIRECTION = 6;
    
	public ReportController() {
	}
	
	public List<ReportTree> getAllReports(HttpServletRequest request) {		
		User loggedUser = (User) request.getSession().getAttribute(SESSION_USER);
		
		List<ReportTree> reportTree = new ArrayList<ReportTree>();
		addNode(reportTree, 99999, "Organization Reports", "Reports.NonAdmin", "5143", "0");
		addNode(reportTree, 99998, "Stock Reports", "Reports.NonAdmin", "0", "0");
		addNode(reportTree, 99997, "Custom Reports", "Reports.NonAdmin", "0", Integer.toString(loggedUser.getId()));
		addNode(reportTree, 99996, "Admin Reports", "Reports.Admin", "0", "0");
		return reportTree;
	}

	private void addNode(List<ReportTree> reportTree, int nodeId, String nodeName, String queryName, String orgId, String userId) {
		ReportTree rep = new ReportTree(nodeId, nodeName, orgId, userId);
		List<Report> reports = mojaveDB.findWithNamedQuery(queryName, orgId, userId);
		for (Report report: reports) {
			rep.getChildren().add(new ReportTree(report.getId(), (nodeId==99999?report.getOrganizationId() + " - ":"") + report.getName(), Short.toString(report.getOrganizationId()), Integer.toString(report.getUserId())));
		}
		if (reports.size()>0) {
			reportTree.add(rep);
		}
	}

	public ReportResult runReport(ReportCriteria reportCriteria) throws NoResultException {
		Report report = (Report) mojaveDB.findWithNamedQuerySingleResult("Reports.ById", reportCriteria.getReportId());
		
		ReportResult result = new ReportResult();
		List<ReportField> fields = getReportFields(reportCriteria.getReportId());
		//lowerCaseFieldName(fields);
		String keyField = getKeyField(fields);
		List<ReportField> displayFields = fields.subList(0, 5);
		result.setFields(displayFields);
		
		// get the data from the view associated with the report
		String fieldNames = getFieldNames(displayFields);
		String reportTable = report.getReportTable();
		String orderBy = reportCriteria.getSortField();
		String orderDirection = reportCriteria.getSortDirection();
		if (orderBy==null || orderBy.isEmpty()) {
			orderBy = keyField;
			orderDirection = "asc";
		}
		int startFrom = 0;
		int fetchSize = 200;
		
		String sqlStatement = "SELECT " + fieldNames + " FROM " + reportTable + " ORDER BY " + orderBy + " " + orderDirection + " OFFSET " + startFrom + " ROWS FETCH NEXT " + fetchSize + " ROWS ONLY";
		System.out.println(sqlStatement);
		List data = mojaveDB.findWithNativeQuery(sqlStatement); 
		result.setRows(data);
		return result;
	}
	
	public List<ReportField> getReportFields(String id) {
		List<ReportField> reportFields = mojaveDB.findWithNamedQuery("Reports.Fields", id);
		return reportFields;
	}

	private String getKeyField(List<ReportField> fields) {
		for (ReportField field: fields) {
			if (field.getKeyField()) return field.getDataField();
		}
		return null;
	}
	
	private String getFieldNames(List<ReportField> fields) {
		StringBuilder fieldNames = new StringBuilder();
		for (ReportField field: fields) {
			fieldNames.append(field.getDataField() + ",");
		}
		fieldNames.deleteCharAt(fieldNames.length()-1);
		return fieldNames.toString();
	}
	
	private void lowerCaseFieldName(List<ReportField> fields) {
		for (ReportField field: fields) {
			// uncapitalize
			field.setDataField(field.getDataField().substring(0,1).toLowerCase()+field.getDataField().substring(1));
		}		
	}

	public ReportTree createReport(ReportTree reportTree) {
		// TODO Auto-generated method stub
		return null;
	}

	public ReportTree update(ReportTree reportTree) {
		// TODO Auto-generated method stub
		return null;
	}

	public void delete(int id) {
		// TODO Auto-generated method stub
		
	}
}
