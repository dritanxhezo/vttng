package com.vintek.services.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ReportTree implements Serializable {
	private static final long serialVersionUID = -7974875065774593015L;
	
	private int id;
	private String label;
	private String orgId;
	private String userId;
	
	private List<ReportTree> children;

	public ReportTree(int id, String label, String orgId, String userId) {
		super();
		this.id = id;
		this.label = label;
		this.orgId = orgId;
		this.userId = userId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public List<ReportTree> getChildren() {
		if (children==null) children = new ArrayList<ReportTree>();
		return children;
	}

	public void setChildren(List<ReportTree> children) {
		this.children = children;
	}
}
