package com.vintek.services.model;

import java.io.Serializable;

public class Login implements Serializable {
	private static final long serialVersionUID = 8102298061190576086L;

	private String userId;
	private String password;
	private String pin;

	public Login() {	
	}
	
	public Login(String userId, String password, String pin) {
		super();
		this.userId = userId;
		this.password = password;
		this.pin = pin;
	}
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
}
