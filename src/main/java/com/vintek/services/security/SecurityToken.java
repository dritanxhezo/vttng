package com.vintek.services.security;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.vintek.database.model.user.Privilege;

/**
 * @author Dritan Xhezo
 */
public class SecurityToken {

	private String sid;   //session id
	private String userId;
	private int currentOrg;
	private List<Privilege> privileges;

	public SecurityToken() {
	}

	public SecurityToken(String userId, int currentOrg, List<Privilege> privileges) {
		super();
		this.sid = generateRandomId();
		this.userId = userId;
		this.currentOrg = currentOrg;
		this.privileges = privileges;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getCurrentOrg() {
		return currentOrg;
	}

	public void setCurrentOrg(int currentOrg) {
		this.currentOrg = currentOrg;
	}
	
	public List<Privilege> getPriviledges() {
		if (this.privileges == null) {
			this.privileges = new ArrayList<Privilege>();
		}
		return this.privileges;		
	}

	public void setPriviledges(List<Privilege> priviledges) {
		this.privileges = priviledges;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}
	private String generateRandomId() {
		return UUID.randomUUID().toString();
	}	
}
