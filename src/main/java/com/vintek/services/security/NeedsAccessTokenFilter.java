package com.vintek.services.security;

import java.io.IOException;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

/*
 * Prevent CSRF by sending an access token when logging in and expecting it in the header for every request.
 */
@Provider @NeedsAccessToken 
public class NeedsAccessTokenFilter implements ContainerRequestFilter {

	private static final String SESSION_USER = "user";
//	private static final String ACCESS_TOKEN = "X-Access-Token";
	private static final String ACCESS_TOKEN = "X-XSRF-TOKEN";

	@Context
	private HttpServletRequest servletRequest;
	
    @Inject
    private Logger log;
	
	
	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		if (servletRequest.getSession().getAttribute(SESSION_USER)==null) {
			requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).entity("Please log in to access this method").build());
		}
		
		String requestAccessToken = requestContext.getHeaderString(ACCESS_TOKEN);
		String sessionAccessToken = (String) servletRequest.getSession().getAttribute(ACCESS_TOKEN);
		log.info("requestAccessToken: " + requestAccessToken + "; sessionAccessToken: " + sessionAccessToken);
/*		
		if (sessionAccessToken==null || sessionAccessToken.isEmpty()) {
			log.warning("A request came in for request to method " + requestContext.getMethod() + " from " + servletRequest.getRemoteAddr() + " but the user was not authenticated"); 
			requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).entity("Please log in to access this method").build());
		} else if (requestAccessToken==null || requestAccessToken.isEmpty() || !requestAccessToken.equalsIgnoreCase(sessionAccessToken)) {
			log.warning("A request came in for request to method " + requestContext.getMethod() + " from " + servletRequest.getRemoteAddr() + " but the access token did not match the session one.");
			// the requester must send the access token in the header and it must match the token in the session.
			requestContext.abortWith(Response.status(Response.Status.BAD_REQUEST).entity("Please provide valid access token to access this method").build());
		}
*/
	}

}
