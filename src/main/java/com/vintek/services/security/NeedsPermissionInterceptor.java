package com.vintek.services.security;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import javax.ws.rs.ext.ReaderInterceptor;
import javax.ws.rs.ext.ReaderInterceptorContext;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.XMLConfiguration;

import com.vintek.database.model.user.Privilege;
import com.vintek.database.model.user.User;

@Provider
public class NeedsPermissionInterceptor implements ReaderInterceptor {
	
    private static final String SESSION_USER = "user";
	private static final String ALL_ORG = "0";

    private static XMLConfiguration config;
    
    static {
    	try {
			config = new XMLConfiguration("com/vintek/services/security/security-config.xml");
		} catch (ConfigurationException e) {
			e.printStackTrace();
		}
    }
    
    @Context
    HttpServletRequest servletRequest;
    
    @Context
    ResourceInfo resourceInfo;
    
	@Override
	public Object aroundReadFrom(ReaderInterceptorContext context) throws IOException, WebApplicationException {
		User user = (User) servletRequest.getSession().getAttribute(SESSION_USER);
		
		String clsName = resourceInfo.getResourceClass().getName(); //getSimpleName();
		String methodName = resourceInfo.getResourceMethod().getName(); //resourceInfo.getResourceMethod().toString();
		System.out.println(clsName);
		System.out.println(methodName);
		
		//String permxpath1 = String.format( "{/class[@name=%s]/method[@name=%s]/userPermission}", clsName, methodName);
		StringBuffer permxpath = new StringBuffer();
		permxpath.append("/class[@name=").append(clsName).append("]/method[@name=").append(methodName).append("]/userPermission");
		String permissionNeeded = config.getString(permxpath.toString());
		System.out.println(permissionNeeded);
		
		if (permissionNeeded==null || hasPermission(user, permissionNeeded)) {
			return context.proceed();
		} else {
			return Response.status(Response.Status.FORBIDDEN).entity("You do not have the permissions to access this resource").build();
		}	
	}

	private boolean hasPermission(User user, String permissionNeeded) {
		Map<String, List<Privilege>> privileges = user.getPrivilegesMap();
		
		String currOrgId = Integer.toString(user.getCurrentOrgId());
		List<Privilege> currPrivs = privileges.containsKey(currOrgId) ? privileges.get(currOrgId) : privileges.get(ALL_ORG);
		for (Privilege privilege : currPrivs) {
			if (privilege.getPermissionName().equalsIgnoreCase(permissionNeeded)) {
				return true;
			}
		}
		return false;
	}
}
