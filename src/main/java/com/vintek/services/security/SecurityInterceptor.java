package com.vintek.services.security;

import java.util.Map;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;

/**
 * @author Dritan Xhezo
 */
@SecuredService @Interceptor
public class SecurityInterceptor {

	@Inject
	private SecurityManager securityManager;

	//@Context
    //HttpServletRequest servletRequest;
	
	@AroundInvoke
	public Object checkSecurity(InvocationContext joinPoint) throws Exception {
		System.out.println("In SecurityInterceptor");

		String className = joinPoint.getTarget().getClass().getName();
		if (className.indexOf("$")!=-1) {
			className = className.substring(0, className.indexOf("$"));
		}
		//String objectName = (className.indexOf("$")!=-1)?className.substring(0, className.indexOf("$")):className;
		//String packageName = joinPoint.getTarget().getClass().getPackage().getName();
		//String objectName = packageName + "." + className; //className.substring(0, className.indexOf("$"));
		String operationName = joinPoint.getMethod().getName();		
		Object[] parameters = joinPoint.getParameters();
		
		Map<String, Object> ctxData = joinPoint.getContextData();
		for(String key: ctxData.keySet()) {
			System.out.println(ctxData.get(key).toString());
		}
		
		System.out.println("op name: " + operationName);
		System.out.println("ob name: " + className);

		/* If the user is not logged in, don't let them use this method */
		/*if (!securityManager.isLoggedIn()) {
			throw new SecurityViolationException();
		}*/
		
		/*
		 * Invoke the method or next Interceptor in the list, if the current
		 * user is allowed.
		 */
		if (!securityManager.isAllowed(className, operationName, parameters)) {
			throw new SecurityViolationException();
		}

		return joinPoint.proceed();
	}
}