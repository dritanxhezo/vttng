package com.vintek.util.farmcache;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.event.Reception;
import javax.enterprise.inject.Produces;
import javax.inject.Named;

import com.vintek.services.security.SecurityToken;

@ApplicationScoped
public class LoginObserver {
	
	// in a real live scenario this would be replaced by a farm cache such as redis. There we can set the expire the security token on a timeout, 
	// or expire on session termination, or on user initiated logout. 
   private Map<String,SecurityToken> loggedUsers;

   @PostConstruct
   public void init() {
	   loggedUsers = new ConcurrentHashMap<String,SecurityToken>();
   }
   
   @Produces
   @Named
   public Collection<SecurityToken> getLoggedUsers() {
      return loggedUsers.values();
   }
   public SecurityToken getLoggedUserToken(String sid) {
	   return loggedUsers.get(sid);
   }
   public void onUserLoggedIn(@Observes(notifyObserver = Reception.IF_EXISTS) @LoginEventType(eventType = "loggedin") final SecurityToken token) {
	   loggedUsers.put(token.getSid(), token);
   }
   public void onUserLoggedout(@Observes(notifyObserver = Reception.IF_EXISTS) @LoginEventType(eventType = "loggedout") final SecurityToken token) {
	   loggedUsers.remove(token);
   }
}
