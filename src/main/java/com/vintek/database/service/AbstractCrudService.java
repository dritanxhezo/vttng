package com.vintek.database.service;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;

/**
 * @author dritan.xhezo
 */
public abstract class AbstractCrudService {	
	abstract EntityManager getEm();

	public <T> T create(T t) {
		this.getEm().persist(t);
		this.getEm().flush();
		this.getEm().refresh(t);
		return t;
	}

	public <T> T find(Class<T> type, Object id) {
		return (T) this.getEm().find(type, id);
	}

	public <T> void delete(Class<T> type, Object id) {
		Object ref = this.getEm().getReference(type, id);
		this.getEm().remove(ref);
	}

	public <T> T update(T t) {
		return (T) this.getEm().merge(t);
	}
	
	@SuppressWarnings("unchecked")
	public <T> List<T> findWithNamedQuery(String namedQueryName) {
		return this.getEm().createNamedQuery(namedQueryName).getResultList();
	}

	public <T> List<T> findWithNamedQuery(String namedQueryName, Map<String, Object> parameters) {
		return findWithNamedQuery(namedQueryName, parameters, 0);
	}

//	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	@SuppressWarnings("unchecked")
	public <T> List<T> findWithNamedQuery(String namedQueryName, String... parameters) {
		Query query = getEm().createNamedQuery(namedQueryName);
		int paramPos = 1;
		for (String p : parameters) {
			query.setParameter(paramPos++, p);
		}
//		query.setHint("org.hibernate.readOnly", "true");
		return query.getResultList();
	}	

	@SuppressWarnings("unchecked")
	public <T> List<T> findWithQuery(String queryString, String... parameters) throws NoResultException {
		Query query = getEm().createQuery(queryString);
		int paramPos = 1;
		for (String p : parameters) {
			query.setParameter(paramPos++, p);
		}
//		query.setHint("org.hibernate.readOnly", "true");
		return query.getResultList();
	}	
	
	@SuppressWarnings("unchecked")
	public <T> T findWithNamedQuerySingleResult(String namedQueryName, String... parameters) throws NonUniqueResultException, EntityNotFoundException, NoResultException {		
		Query query = getEm().createNamedQuery(namedQueryName);
		int paramPos = 1;
		for (String p : parameters) {
			query.setParameter(paramPos++, p);
		}			
		return (T) query.getSingleResult();
	}

	@SuppressWarnings("unchecked")
	public <T> T findWithQuerySingleResult(String queryString, String... parameters) throws NonUniqueResultException, EntityNotFoundException, NoResultException {
		Query query = getEm().createQuery(queryString);
		int paramPos = 1;
		for (String p : parameters) {
			query.setParameter(paramPos++, p);
		}
		return (T) query.getSingleResult();
	}	

	@SuppressWarnings("unchecked")
	public <T> List<T> findWithNamedQuery(String queryName, int resultLimit) {
		return this.getEm().createNamedQuery(queryName).setMaxResults(resultLimit).getResultList();
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> findByNativeQuery(String sql, Class<T> type) {
		return this.getEm().createNativeQuery(sql, type).getResultList();
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> findWithNativeQuery(String sql) {
		return this.getEm().createNativeQuery(sql).getResultList();
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> findWithNamedQuery(String namedQueryName, Map<String, Object> parameters, int resultLimit) {
		Set<Entry<String, Object>> rawParameters = parameters.entrySet();
		Query query = this.getEm().createNamedQuery(namedQueryName);
		if (resultLimit > 0)
			query.setMaxResults(resultLimit);
		for (Entry<String, Object> entry : rawParameters) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		return query.getResultList();
	}

	public <T> List<T> findWithCriteria(CriteriaQuery<T> criteria) {
		return getEm().createQuery(criteria).getResultList();
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> findWithCriteria(CriteriaQuery<T> criteria, int resultLimit) {
		Query query = this.getEm().createQuery(criteria);
		query.setMaxResults(resultLimit);
		return query.getResultList();
	}
}
