package com.vintek.database.model.loan;

import java.io.Serializable;

import javax.persistence.*;

/**
 * The persistent class for the StateCollateralTransaction database table.
 * 
 */
@Entity
public class StateCollateralTransaction implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private int id;

	@Column(name="CollateralID")
	private int collateralId;

	//bi-directional many-to-one association to StateTransactionError
//	@OneToMany(mappedBy="stateCollateralTransaction")
//	private List<StateTransactionError> stateTransactionErrors;

	@Column(name="TransactionID")
	private int transactionId;

	public StateCollateralTransaction() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCollateralId() {
		return this.collateralId;
	}

	public void setCollateralId(int collateralId) {
		this.collateralId = collateralId;
	}

	public int getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

//	public List<StateTransactionError> getStateTransactionErrors() {
//		return this.stateTransactionErrors;
//	}
//
//	public void setStateTransactionErrors(List<StateTransactionError> stateTransactionErrors) {
//		this.stateTransactionErrors = stateTransactionErrors;
//	}
//
//	public StateTransactionError addStateTransactionError(StateTransactionError stateTransactionError) {
//		getStateTransactionErrors().add(stateTransactionError);
//		stateTransactionError.setStateCollateralTransaction(this);
//
//		return stateTransactionError;
//	}
//
//	public StateTransactionError removeStateTransactionError(StateTransactionError stateTransactionError) {
//		getStateTransactionErrors().remove(stateTransactionError);
//		stateTransactionError.setStateCollateralTransaction(null);
//
//		return stateTransactionError;
//	}

}