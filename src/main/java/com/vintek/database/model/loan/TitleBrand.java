package com.vintek.database.model.loan;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the TitleBrand database table.
 * 
 */
@Entity
public class TitleBrand implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private int id;

	@Column(name="Brand")
	@Lob
	private String brand;

	//bi-directional many-to-one association to Title
//	@ManyToOne
//	@JoinColumn(name="TitleID")
//	private Title title;

	@Column(name="TitleID")
	private int titleId;
	
	public TitleBrand() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBrand() {
		return this.brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public int getTitleId() {
		return this.titleId;
	}

	public void setTitleId(int titleId) {
		this.titleId = titleId;
	}

}