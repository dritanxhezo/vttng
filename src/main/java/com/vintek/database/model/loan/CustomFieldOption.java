package com.vintek.database.model.loan;

import java.io.Serializable;

import javax.persistence.*;


/**
 * The persistent class for the CustomFieldOption database table.
 * 
 */
@Entity
public class CustomFieldOption implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private int id;
	
	@Column(name="CustomFieldID")
	private int customFieldID;

	@Column(name="OptionValue")
	private String optionValue;

	public CustomFieldOption() {
	}

	public int getCustomFieldID() {
		return this.customFieldID;
	}

	public void setCustomFieldID(int customFieldID) {
		this.customFieldID = customFieldID;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getOptionValue() {
		return this.optionValue;
	}

	public void setOptionValue(String optionValue) {
		this.optionValue = optionValue;
	}

}