package com.vintek.database.model.loan;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.Date;


/**
 * The persistent class for the LoanHistoryNew database table. 
 */
@Entity
@Table(name="LoanHistoryNew")
public class LoanHistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="ChangeDate")
	private Date changeDate;

	@Column(name="ChangeUser")
	private String changeUser;

	@OneToOne(cascade=CascadeType.ALL,fetch=FetchType.EAGER,orphanRemoval=true) @JoinColumn(name="NoteID", referencedColumnName="ID", insertable=true,updatable=true,nullable=true,unique=true)
	//@Column(name="NoteID")
	private Note note;

	@Column(columnDefinition="bit")
	private boolean urgent;

	//bi-directional many-to-one association to Loan
//	@ManyToOne
//	@JoinColumn(name="LoanID")
//	private Loan loan;
	@Column(name="LoanID")
	private int loanId;

	public LoanHistory() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getChangeDate() {
		return this.changeDate;
	}

	public void setChangeDate(Date changeDate) {
		this.changeDate = changeDate;
	}

	public String getChangeUser() {
		return this.changeUser;
	}

	public void setChangeUser(String changeUser) {
		this.changeUser = changeUser;
	}

	public Note getNote() {
		return this.note;
	}

	public void setNote(Note note) {
		this.note = note;
	}

	public boolean getUrgent() {
		return this.urgent;
	}

	public void setUrgent(boolean urgent) {
		this.urgent = urgent;
	}

//	public Loan getLoan() {
//		return this.loan;
//	}
//
//	public void setLoan(Loan loan) {
//		this.loan = loan;
//	}
	public int getLoanId() {
		return this.loanId;
	}

	public void setLoanId(int loanId) {
		this.loanId = loanId;
	}

}