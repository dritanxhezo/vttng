package com.vintek.database.model.loan;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;


/**
 * The persistent class for the TitleHistoryNew database table.
 * 
 */
@Entity
@Table(name="TitleHistoryNew")
public class TitleHistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private int id;

	@Column(name="ChangeDate")
	private Timestamp changeDate;

	@Column(name="ChangeUser")
	private String changeUser;

	@Column(columnDefinition="bit")
	private boolean urgent;

	//bi-directional many-to-one association to Note
//	@ManyToOne
//	@JoinColumn(name="NoteID")
//	private Note note;

	@OneToOne(cascade=CascadeType.ALL,fetch=FetchType.EAGER,orphanRemoval=true) @JoinColumn(name="NoteID", referencedColumnName="ID", insertable=true,updatable=true,nullable=true,unique=true)
	private Note note;
	
	//bi-directional many-to-one association to Title
//	@ManyToOne
//	@JoinColumn(name="TitleID")
//	private Title title;
	@Column(name="TitleID")
	private int titleId;
	
	public TitleHistory() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getChangeDate() {
		return this.changeDate;
	}

	public void setChangeDate(Timestamp changeDate) {
		this.changeDate = changeDate;
	}

	public String getChangeUser() {
		return this.changeUser;
	}

	public void setChangeUser(String changeUser) {
		this.changeUser = changeUser;
	}

	public boolean getUrgent() {
		return this.urgent;
	}

	public void setUrgent(boolean urgent) {
		this.urgent = urgent;
	}

	public Note getNote() {
		return this.note;
	}

	public void setNote(Note note) {
		this.note = note;
	}

	public int getTitleId() {
		return this.titleId;
	}

	public void setTitleId(int titleId) {
		this.titleId = titleId;
	}

}