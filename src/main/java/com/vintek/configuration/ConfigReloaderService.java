package com.vintek.configuration;

import java.util.logging.Logger;

import javax.inject.Inject;
import javax.naming.InitialContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import com.vintek.database.model.user.PermissionConst;
import com.vintek.database.model.user.Privilege;
import com.vintek.database.model.user.User;
import com.vintek.services.rest.ServiceException;

/**
 * @author dritan.xhezo
 */
@Path("/reloadlocalconfig")
public class ConfigReloaderService {
	
    @Inject
    private Logger log;
	
	private static final String JNDI_APP_PROPERTIES = "java:app/vttng/AppProperties";
	private static final String SESSION_USER = "user";
	
	@GET
	public Response updateConfig(@Context HttpServletRequest request, @Context HttpServletResponse response) {
		try {
			reloadLocalConfig(request);
			return Response.status(Response.Status.OK).entity("Reloaded config from the local host.").build();
		} catch (ServiceException se) {
			log.severe("Problem reloading the local config for this application. " + se.getMessage());
			return Response.status(se.getResponseStatus()).entity(se.getMessage()).build();
		} catch (Exception e) {
			log.severe("Problem reloading the local config for this application. "  + e.getMessage());
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("An Exception occurred while trying to reload the config from the local host.").build();
		}
	}
	
	private void reloadLocalConfig(HttpServletRequest request) throws ServiceException {
		User loggedUser = (User)request.getSession().getAttribute(SESSION_USER);
		Privilege p = (Privilege) loggedUser.getPrivilegesMap().get("0");  // is admin in the all org permission level?
		if (PermissionConst.DB_PRIVILEGE_ADMIN.equalsIgnoreCase(p.getPermissionName())) {
			try {
				AppProperties appProperties = (AppProperties) new InitialContext().lookup(JNDI_APP_PROPERTIES);
				appProperties.loadProperties();
			} catch (Exception e) {
				log.severe("Could not get a handle on the AppProperties; " + e.getMessage());
				throw new ServiceException(Response.Status.INTERNAL_SERVER_ERROR, "An exception occurred while servicing this request");
			}
		} else {
			throw new ServiceException(Response.Status.UNAUTHORIZED, "User has no privileges to perform this action");
		}
	}

}