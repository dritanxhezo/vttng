package com.vintek.configuration;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import java.util.logging.Logger;

/**
 * @author dritan.xhezo
 */
/**
 * Copy the <application>.properties in the jboss standalone configuration.
 * for managed domain use: jboss.domain.config.dir
 */
@Singleton
@Startup
public class AppProperties {
	
    @Inject
    private Logger log;
	
	private String appPropertiesFilePath = "";
	private Properties properties = new Properties();

	@PostConstruct
	public void init() {
		try {
			String appName = (String) new InitialContext().lookup("java:app/AppName");
		    appPropertiesFilePath = System.getProperty("jboss.server.config.dir") + File.separator + appName +  "-application.properties";
		    loadProperties();
		} catch (NamingException e) {
			log.severe("Property file not found for this application. Error: " + e.getMessage());
		}
	}
	
	public void loadProperties() {
		properties.clear();
		try {
			FileInputStream fis = new FileInputStream(appPropertiesFilePath);
			properties.load(fis);
			fis.close();
			log.info("Loaded application property file " + appPropertiesFilePath);
		} catch (FileNotFoundException e) {
			log.severe("Property file " + appPropertiesFilePath + " not found. Error: " + e.getMessage());
		} catch (IOException e) {
			log.severe("Error reading property file " + appPropertiesFilePath + ". Error: " + e.getMessage());
		}
	}
	
	public String getProperty(String key) {
		if ( properties.isEmpty() ) {
			return "";
		}
		String val = properties.getProperty(key);
		if ( val == null ) {
			return "";
		}
		return val.trim();
	}

	public void setProperty(String key, String value) {
		if (properties.size() > 0) {
			properties.setProperty(key, value.trim());
		}
	}
	
	public void save(String userName) {
		try {
			FileOutputStream fos = new FileOutputStream(appPropertiesFilePath);
			properties.store(fos, "Updated by: " + userName);
			fos.close();
			log.info("Saved application property file " + appPropertiesFilePath);
		} catch (FileNotFoundException e) {
			log.severe("Property file " + appPropertiesFilePath + " not found. Error: " + e.getMessage());
		} catch (IOException e) {
			log.severe("Error reading property file " + appPropertiesFilePath + ". Error: " + e.getMessage());
		} 
	}
}


